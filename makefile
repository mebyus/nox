.PHONY: default
default: fmt test build

.PHONY: build
build: vm asn lnx

.PHONY: vm
vm:
	go build -o bin/vm ./vm

.PHONY: asn
asn:
	go build -o bin/asn ./asn

.PHONY: lnx
lnx:
	go build -o bin/lnx ./lnx

.PHONY: fmt
fmt:
	go fmt ./...

.PHONY: test
test:
	go test ./...

# NOX Programming language

vm - Virtual Machine for executing Nox code

asn - assembler for nox vm

.nvm - file extension of nox compiled code for virtual machine

onx - format of assembled (object) nox code

/onx - onx file encoder and decoder

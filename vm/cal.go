package main

import "codeberg.org/mebyus/nox/vm/opcode"

func (vm *VirtualMachine) cal(addr uint16, returnAddress uint16) error {
	err := vm.psh(returnAddress)
	if err != nil {
		return err
	}
	err = vm.psh(vm.fp)
	if err != nil {
		return err
	}
	vm.fp = vm.sp

	err = vm.jmp(addr)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 2
func (vm *VirtualMachine) calLit(data []byte) error {
	address := getUint16(data[0:])
	err := vm.cal(address, vm.ip+opcode.Size[opcode.CalLit])
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 1
func (vm *VirtualMachine) calReg(data []byte) error {
	register := data[0]
	address, err := vm.get(register)
	if err != nil {
		return err
	}
	err = vm.cal(address, vm.ip+opcode.Size[opcode.CalReg])
	if err != nil {
		return err
	}
	return nil
}

package main

import (
	"codeberg.org/mebyus/nox/sys"
	"codeberg.org/mebyus/nox/vm/opcode"
)

func (vm *VirtualMachine) step() {
	opc := opcode.OpCode(vm.code[vm.ip])
	mnemo := opcode.ToMnemo[opc]
	size := opcode.Size[opc]
	if size == 0 {
		sys.Fatalf("unknown opcode 0x%02X at 0x%04X", opc, vm.ip)
	}
	if vm.ip+size > vm.codelen {
		sys.Fatalf("not enough code to read %d bytes for %s instruction (0x%02X) at 0x%04X", size, mnemo, opc, vm.ip)
	}

	// instruction data excluding opcode
	data := vm.code[vm.ip+1 : vm.ip+size]

	vm.jump = false
	var err error
	switch opc {
	case opcode.Nop:
		display(vm)
	case opcode.Hlt:
		vm.hlt()
		return
	case opcode.MovLitReg:
		err = vm.movLitReg(data)
	case opcode.MovRegReg:
		err = vm.movRegReg(data)
	case opcode.MovMemReg:
		err = vm.movMemReg(data)
	case opcode.MovRegMem:
		err = vm.movRegMem(data)
	case opcode.MovLitMem:
		err = vm.movLitMem(data)
	case opcode.MovRegPtrReg:
		err = vm.movRegPtrReg(data)
	case opcode.MovLitOffReg:
		err = vm.movLitOffReg(data)
	case opcode.AddRegReg:
		err = vm.addRegReg(data)
	case opcode.AddLitReg:
		err = vm.addLitReg(data)
	case opcode.SubRegLit:
		err = vm.subRegLit(data)
	case opcode.SubLitReg:
		err = vm.subLitReg(data)
	case opcode.SubRegReg:
		err = vm.subRegReg(data)
	case opcode.Inc:
		err = vm.inc(data)
	case opcode.Dec:
		err = vm.dec(data)
	case opcode.LshRegLit:
		err = vm.lshRegLit(data)
	case opcode.LshRegReg:
		err = vm.lshRegReg(data)
	case opcode.RshRegLit:
		err = vm.rshRegLit(data)
	case opcode.RshRegReg:
		err = vm.rshRegReg(data)
	case opcode.AndRegLit:
		err = vm.andRegLit(data)
	case opcode.AndRegReg:
		err = vm.andRegReg(data)
	case opcode.OrRegLit:
		err = vm.orRegLit(data)
	case opcode.OrRegReg:
		err = vm.orRegReg(data)
	case opcode.XorRegLit:
		err = vm.xorRegLit(data)
	case opcode.XorRegReg:
		err = vm.xorRegReg(data)
	case opcode.Not:
		err = vm.not(data)
	case opcode.JneLit:
		err = vm.jneLit(data)
	case opcode.JneReg:
		err = vm.jneReg(data)
	case opcode.JeqLit:
		err = vm.jeqLit(data)
	case opcode.JeqReg:
		err = vm.jeqReg(data)
	case opcode.JltLit:
		err = vm.jltLit(data)
	case opcode.JltReg:
		err = vm.jltReg(data)
	case opcode.JgtLit:
		err = vm.jgtLit(data)
	case opcode.JgtReg:
		err = vm.jgtReg(data)
	case opcode.JleLit:
		err = vm.jleLit(data)
	case opcode.JleReg:
		err = vm.jleReg(data)
	case opcode.JgeLit:
		err = vm.jgeLit(data)
	case opcode.JgeReg:
		err = vm.jgeReg(data)
	case opcode.Jmp:
		err = vm.jmpInst(data)
	case opcode.PshLit:
		err = vm.pshLit(data)
	case opcode.PshReg:
		err = vm.pshReg(data)
	case opcode.PopReg:
		err = vm.popReg(data)
	case opcode.CalLit:
		err = vm.calLit(data)
	case opcode.CalReg:
		err = vm.calReg(data)
	case opcode.Ret:
		err = vm.ret(data)
	default:
		sys.Fatalf("unknown opcode 0x%02X at 0x%04X", opc, vm.ip)
	}

	if err != nil {
		display(vm)
		sys.Fatalf("%s instruction (0x%02X) at 0x%04X: %v", mnemo, opc, vm.ip, err)
	}

	if !vm.jump {
		vm.ip += size
	}
}

func (vm *VirtualMachine) hlt() {
	vm.halt = true
}

package main

// len(data) = 1
func (vm *VirtualMachine) inc(data []byte) error {
	register := data[0]
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	value++
	err = vm.set(register, value)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 1
func (vm *VirtualMachine) dec(data []byte) error {
	register := data[0]
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	value--
	err = vm.set(register, value)
	if err != nil {
		return err
	}
	return nil
}

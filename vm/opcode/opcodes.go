package opcode

import "codeberg.org/mebyus/nox/vm/mnemo"

type OpCode uint8

const (
	Nop OpCode = 0x00

	MovLitReg    OpCode = 0x10
	MovRegReg    OpCode = 0x11
	MovMemReg    OpCode = 0x12
	MovRegMem    OpCode = 0x13
	MovLitMem    OpCode = 0x14
	MovRegPtrReg OpCode = 0x15
	MovLitOffReg OpCode = 0x16

	AddRegReg OpCode = 0x20
	AddLitReg OpCode = 0x21

	SubRegLit OpCode = 0x28
	SubLitReg OpCode = 0x29
	SubRegReg OpCode = 0x30

	Inc OpCode = 0x38
	Dec OpCode = 0x39

	LshRegLit OpCode = 0x40
	LshRegReg OpCode = 0x41
	RshRegLit OpCode = 0x42
	RshRegReg OpCode = 0x43

	AndRegLit OpCode = 0x44
	AndRegReg OpCode = 0x45
	OrRegLit  OpCode = 0x46
	OrRegReg  OpCode = 0x47
	XorRegLit OpCode = 0x48
	XorRegReg OpCode = 0x49
	Not       OpCode = 0x4A

	JneLit OpCode = 0x50
	JneReg OpCode = 0x51
	JeqLit OpCode = 0x52
	JeqReg OpCode = 0x53
	JltLit OpCode = 0x54
	JltReg OpCode = 0x55
	JgtLit OpCode = 0x56
	JgtReg OpCode = 0x57
	JleLit OpCode = 0x58
	JleReg OpCode = 0x59
	JgeLit OpCode = 0x5A
	JgeReg OpCode = 0x5B
	Jmp    OpCode = 0x5C

	PshLit OpCode = 0x70
	PshReg OpCode = 0x71

	PopReg OpCode = 0x7A

	CalLit OpCode = 0x80
	CalReg OpCode = 0x81

	Ret OpCode = 0x8A

	Hlt OpCode = 0xFF
)

var ToMnemo = [...]mnemo.Mnemo{
	Nop: mnemo.Nop,

	MovLitReg:    mnemo.Mov,
	MovRegReg:    mnemo.Mov,
	MovMemReg:    mnemo.Mov,
	MovRegMem:    mnemo.Mov,
	MovLitMem:    mnemo.Mov,
	MovRegPtrReg: mnemo.Mov,
	MovLitOffReg: mnemo.Mov,

	AddRegReg: mnemo.Add,
	AddLitReg: mnemo.Add,

	SubRegLit: mnemo.Sub,
	SubLitReg: mnemo.Sub,
	SubRegReg: mnemo.Sub,

	Inc: mnemo.Inc,
	Dec: mnemo.Dec,

	LshRegLit: mnemo.Lsh,
	LshRegReg: mnemo.Lsh,
	RshRegLit: mnemo.Rsh,
	RshRegReg: mnemo.Rsh,

	AndRegLit: mnemo.And,
	AndRegReg: mnemo.And,
	OrRegLit:  mnemo.Or,
	OrRegReg:  mnemo.Or,
	XorRegLit: mnemo.Xor,
	XorRegReg: mnemo.Xor,
	Not:       mnemo.Not,

	JneLit: mnemo.Jne,
	JneReg: mnemo.Jne,
	JeqLit: mnemo.Jeq,
	JeqReg: mnemo.Jeq,
	JltLit: mnemo.Jlt,
	JltReg: mnemo.Jlt,
	JgtLit: mnemo.Jgt,
	JgtReg: mnemo.Jgt,
	JleLit: mnemo.Jle,
	JleReg: mnemo.Jle,
	JgeLit: mnemo.Jge,
	JgeReg: mnemo.Jge,
	Jmp:    mnemo.Jmp,

	PshLit: mnemo.Psh,
	PshReg: mnemo.Psh,

	PopReg: mnemo.Pop,

	CalLit: mnemo.Cal,
	CalReg: mnemo.Cal,

	Ret: mnemo.Ret,

	Hlt: mnemo.Hlt,
}

// Size allows to obtain instruction size denoted
// by specific opcode.
//
// In addition layout of each instruction is documented here.
// Layout includes byte sizes of each instruction segment
// and their order.
//
// # C - 1 byte - opcode
//
// # R - 1 byte - register
//
// # L - 2 bytes - literal
//
// # A - 2 bytes - address in memory
//
// P - 2 bytes - position (address) in code (for jump instructions)
var Size = [...]uint16{
	// C
	Nop: 1,

	// C + R + L
	MovLitReg: 1 + 1 + 2,

	// C + R + R
	MovRegReg: 1 + 1 + 1,

	// C + R + A
	MovMemReg: 1 + 1 + 2,

	// C + R + A
	MovRegMem: 1 + 1 + 2,

	// C + L + A
	MovLitMem: 1 + 2 + 2,

	// C + R + R
	MovRegPtrReg: 1 + 1 + 1,

	// C + R + R + L
	MovLitOffReg: 1 + 1 + 1 + 2,

	// C + R + R
	AddRegReg: 1 + 1 + 1,

	// C + R + L
	AddLitReg: 1 + 1 + 2,

	// C + R + L
	SubRegLit: 1 + 1 + 2,

	// C + R + L
	SubLitReg: 1 + 1 + 2,

	// C + R + R
	SubRegReg: 1 + 1 + 1,

	// C + R
	Inc: 1 + 1,

	// C + R
	Dec: 1 + 1,

	// C + R + L
	LshRegLit: 1 + 1 + 2,

	// C + R + R
	LshRegReg: 1 + 1 + 1,

	// C + R + L
	RshRegLit: 1 + 1 + 2,

	// C + R + R
	RshRegReg: 1 + 1 + 1,

	// C + R + L
	AndRegLit: 1 + 1 + 2,

	// C + R + R
	AndRegReg: 1 + 1 + 1,

	// C + R + L
	OrRegLit: 1 + 1 + 2,

	// C + R + R
	OrRegReg: 1 + 1 + 1,

	// C + R + L
	XorRegLit: 1 + 1 + 2,

	// C + R + R
	XorRegReg: 1 + 1 + 1,

	// C + R
	Not: 1 + 1,

	// C + L + P
	JneLit: 1 + 2 + 2,

	// C + R + P
	JneReg: 1 + 1 + 2,

	// C + L + P
	JeqLit: 1 + 2 + 2,

	// C + R + P
	JeqReg: 1 + 1 + 2,

	// C + L + P
	JltLit: 1 + 2 + 2,

	// C + R + P
	JltReg: 1 + 1 + 2,

	// C + L + P
	JgtLit: 1 + 2 + 2,

	// C + R + P
	JgtReg: 1 + 1 + 2,

	// C + L + P
	JleLit: 1 + 2 + 2,

	// C + R + P
	JleReg: 1 + 1 + 2,

	// C + L + P
	JgeLit: 1 + 2 + 2,

	// C + R + P
	JgeReg: 1 + 1 + 2,

	// C + P
	Jmp: 1 + 2,

	// C + L
	PshLit: 1 + 2,

	// C + R
	PshReg: 1 + 1,

	// C + R
	PopReg: 1 + 1,

	// C + L
	CalLit: 1 + 2,

	// C + R
	CalReg: 1 + 1,

	// C
	Ret: 1,

	// C
	Hlt: 1,
}

package main

import (
	"encoding/binary"
	"fmt"
)

var (
	ErrOutOfBoundsMemoryAccess = fmt.Errorf("out of bounds memory access")
)

func getUint16(b []byte) uint16 {
	return binary.LittleEndian.Uint16(b)
}

func putUint16(b []byte, v uint16) {
	binary.LittleEndian.PutUint16(b, v)
}

func (vm *VirtualMachine) memget(addr uint16) (uint16, error) {
	if addr >= memorySize-1 {
		return 0, ErrOutOfBoundsMemoryAccess
	}
	value := getUint16(vm.mem[addr:])
	return value, nil
}

func (vm *VirtualMachine) memset(addr, value uint16) error {
	if addr >= memorySize-1 {
		return ErrOutOfBoundsMemoryAccess
	}
	putUint16(vm.mem[addr:], value)
	return nil
}

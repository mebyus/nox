package main

// len(data) = 1
func (vm *VirtualMachine) not(data []byte) error {
	register := data[0]
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	vm.ac = ^value
	return nil
}

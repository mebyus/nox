package main

import (
	"fmt"
)

const (
	memorySize    = 1 << 16
	stackSize     = 1 << 12
	stackBoundary = memorySize - stackSize
)

type VirtualMachine struct {
	CPU

	// memory
	mem [memorySize]byte

	// Execution data and flags

	// length of executed program
	codelen uint16

	// flag, indicates if jump occured during current instruction
	jump bool

	// flag, indicates if vm was halted by instruction
	halt bool

	// code being executed by vm
	code []byte
}

func NewVM() *VirtualMachine {
	return &VirtualMachine{}
}

func (vm *VirtualMachine) Exec(code []byte) {
	vm.halt = false
	vm.code = code
	vm.codelen = uint16(len(code))

	// zero vm registers in case Exec is called more than once
	vm.ip = 0
	vm.ac = 0

	// initialize stack registers, so they point to the end of the memory
	vm.sp = memorySize - 2
	vm.fp = vm.sp

	for !vm.halt && vm.ip < vm.codelen {
		vm.step()
	}

	vm.exit()
}

func (vm *VirtualMachine) exit() {
	display(vm)
	if vm.halt {
		fmt.Printf("halted at 0x%04X\n", vm.ip)
		return
	}
	if vm.ip >= vm.codelen {
		fmt.Printf("halted after reaching end of code at 0x%04X\n", vm.ip)
		return
	}

	panic("unreachable exit")
}

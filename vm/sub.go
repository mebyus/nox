package main

// len(data) = 3
func (vm *VirtualMachine) subRegLit(data []byte) error {
	register := data[0]
	literal := getUint16(data[1:])
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	vm.ac = value - literal
	return nil
}

// len(data) = 3
func (vm *VirtualMachine) subLitReg(data []byte) error {
	register := data[0]
	literal := getUint16(data[1:])
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	vm.ac = literal - value
	return nil
}

// len(data) = 2
func (vm *VirtualMachine) subRegReg(data []byte) error {
	register1 := data[0]
	register2 := data[1]
	value1, err := vm.get(register1)
	if err != nil {
		return err
	}
	value2, err := vm.get(register2)
	if err != nil {
		return err
	}
	vm.ac = value1 - value2
	return nil
}

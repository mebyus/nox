package main

import "fmt"

func display(vm *VirtualMachine) {
	fmt.Printf(`
	ip: 0x%04X
	sp: 0x%04X
	fp: 0x%04X	ac: 0x%04X

	r1: 0x%04X	r2: 0x%04X
	r3: 0x%04X	r4: 0x%04X
	r5: 0x%04X	r6: 0x%04X
	r7: 0x%04X	r8: 0x%04X

`,
		vm.ip, vm.sp, vm.fp, vm.ac,
		vm.r1, vm.r2,
		vm.r3, vm.r4,
		vm.r5, vm.r6,
		vm.r7, vm.r8,
	)

	displayMem(memorySize-1, vm)
	displayCode(vm.ip, vm.code)
}

var memDisplayRuler = [...]byte{
	0x00,
	0x01,
	0x02,
	0x03,
	0x04,
	0x05,
	0x06,
	0x07,
	0x08,
	0x09,
	0x0A,
	0x0B,
	0x0C,
	0x0D,
	0x0E,
	0x0F,
}

func displayCode(ip uint16, code []byte) {
	start := ip & 0xFFF0
	lines := (len(code)-int(start))/0x10 + 1
	for i := 0; i < lines*0x0010; i += 0x0010 {
		blockStart := int(start) + i
		blockEnd := blockStart + 0x0010
		if blockEnd > len(code) {
			blockEnd = len(code)
		}
		block := code[blockStart:blockEnd]
		fmt.Printf("0x%04X:  % X\n", blockStart, block)
	}
	fmt.Println()
}

func displayMem(addr uint16, vm *VirtualMachine) {
	start := addr & 0xFFF0
	if start < 0x0080 {
		start = 0
	} else if start > 0xFF80 {
		start = 0xFF00
	} else {
		start = start - 0x0080
	}
	fmt.Printf("         % X\n\n", memDisplayRuler)
	for i := 0; i < 0x0100; i += 0x0010 {
		block := int(start) + i
		fmt.Printf("0x%04X:  % X\n", block, vm.mem[block:block+0x0010])
	}
	fmt.Println()
}

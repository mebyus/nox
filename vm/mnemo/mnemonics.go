package mnemo

type Mnemo = string

// List of asn mnemonics
const (
	Nop Mnemo = "nop"
	Mov Mnemo = "mov"
	Add Mnemo = "add"
	Sub Mnemo = "sub"
	Inc Mnemo = "inc"
	Dec Mnemo = "dec"
	Lsh Mnemo = "lsh"
	Rsh Mnemo = "rsh"
	And Mnemo = "and"
	Or  Mnemo = "or"
	Xor Mnemo = "xor"
	Not Mnemo = "not"
	Jne Mnemo = "jne"
	Jeq Mnemo = "jeq"
	Jlt Mnemo = "jlt"
	Jgt Mnemo = "jgt"
	Jle Mnemo = "jle"
	Jge Mnemo = "jge"
	Jmp Mnemo = "jmp"
	Psh Mnemo = "psh"
	Pop Mnemo = "pop"
	Cal Mnemo = "cal"
	Ret Mnemo = "ret"
	Hlt Mnemo = "hlt"
)

type Code = int

const (
	CodeNop Code = iota
	CodeMov
	CodeAdd
	CodeSub
	CodeInc
	CodeDec
	CodeLsh
	CodeRsh
	CodeAnd
	CodeOr
	CodeXor
	CodeNot
	CodeJne
	CodeJeq
	CodeJlt
	CodeJgt
	CodeJle
	CodeJge
	CodeJmp
	CodePsh
	CodePop
	CodeCal
	CodeRet
	CodeHlt
)

var ToCode = map[Mnemo]Code{
	Nop: CodeNop,
	Mov: CodeMov,
	Add: CodeAdd,
	Sub: CodeSub,
	Inc: CodeInc,
	Dec: CodeDec,
	Lsh: CodeLsh,
	Rsh: CodeRsh,
	And: CodeAnd,
	Or:  CodeOr,
	Xor: CodeXor,
	Not: CodeNot,
	Jne: CodeJne,
	Jeq: CodeJeq,
	Jlt: CodeJlt,
	Jgt: CodeJgt,
	Jle: CodeJle,
	Jge: CodeJge,
	Jmp: CodeJmp,
	Psh: CodePsh,
	Pop: CodePop,
	Cal: CodeCal,
	Ret: CodeRet,
	Hlt: CodeHlt,
}

var ToMnemo = [...]Mnemo{
	CodeNop: Nop,
	CodeMov: Mov,
	CodeAdd: Add,
	CodeSub: Sub,
	CodeInc: Inc,
	CodeDec: Dec,
	CodeLsh: Lsh,
	CodeRsh: Rsh,
	CodeAnd: And,
	CodeOr:  Or,
	CodeXor: Xor,
	CodeNot: Not,
	CodeJne: Jne,
	CodeJeq: Jeq,
	CodeJlt: Jlt,
	CodeJgt: Jgt,
	CodeJle: Jle,
	CodeJge: Jge,
	CodeJmp: Jmp,
	CodePsh: Psh,
	CodePop: Pop,
	CodeCal: Cal,
	CodeRet: Ret,
	CodeHlt: Hlt,
}

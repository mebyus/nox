package main

// len(data) = 3
func (vm *VirtualMachine) rshRegLit(data []byte) error {
	register := data[0]
	literal := getUint16(data[1:])
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	if literal < 1 || literal > 15 {
		return ErrShiftOutOfRange
	}
	value >>= literal
	err = vm.set(register, value)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 2
func (vm *VirtualMachine) rshRegReg(data []byte) error {
	register1 := data[0]
	register2 := data[1]
	value, err := vm.get(register1)
	if err != nil {
		return err
	}
	shift, err := vm.get(register2)
	if err != nil {
		return err
	}
	if shift > 16 {
		return ErrShiftOutOfRange
	}
	value >>= shift
	err = vm.set(register1, value)
	if err != nil {
		return err
	}
	return nil
}

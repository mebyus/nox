package main

import "fmt"

var (
	ErrEmptyStackPop = fmt.Errorf("empty stack pop")
)

func (vm *VirtualMachine) pop() (value uint16, err error) {
	if vm.sp >= memorySize-2 {
		return 0, ErrEmptyStackPop
	}
	vm.sp += 2
	value = getUint16(vm.mem[vm.sp:])
	return value, nil
}

// len(data) = 1
func (vm *VirtualMachine) popReg(data []byte) error {
	value, err := vm.pop()
	if err != nil {
		return err
	}
	register := data[0]
	err = vm.set(register, value)
	if err != nil {
		return err
	}
	return nil
}

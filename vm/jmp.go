package main

import (
	"fmt"
)

var (
	ErrOutOfBoundsCodeAddress = fmt.Errorf("out of bounds code address")
)

func (vm *VirtualMachine) jmp(addr uint16) error {
	if addr >= vm.codelen {
		return ErrOutOfBoundsCodeAddress
	}
	vm.ip = addr
	vm.jump = true
	return nil
}

// len(data) = 2
func (vm *VirtualMachine) jmpInst(data []byte) error {
	address := getUint16(data[0:])
	err := vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 4
func (vm *VirtualMachine) jneLit(data []byte) error {
	value := getUint16(data[0:])
	address := getUint16(data[2:])
	if vm.ac == value {
		return nil
	}
	err := vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 3
func (vm *VirtualMachine) jneReg(data []byte) error {
	register := data[0]
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	address := getUint16(data[1:])
	if vm.ac == value {
		return nil
	}
	err = vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 4
func (vm *VirtualMachine) jeqLit(data []byte) error {
	value := getUint16(data[0:])
	address := getUint16(data[2:])
	if vm.ac != value {
		return nil
	}
	err := vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 3
func (vm *VirtualMachine) jeqReg(data []byte) error {
	register := data[0]
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	address := getUint16(data[1:])
	if vm.ac != value {
		return nil
	}
	err = vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 4
func (vm *VirtualMachine) jltLit(data []byte) error {
	value := getUint16(data[0:])
	address := getUint16(data[2:])
	if !(vm.ac < value) {
		return nil
	}
	err := vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 3
func (vm *VirtualMachine) jltReg(data []byte) error {
	register := data[0]
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	address := getUint16(data[1:])
	if !(vm.ac < value) {
		return nil
	}
	err = vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 4
func (vm *VirtualMachine) jgtLit(data []byte) error {
	value := getUint16(data[0:])
	address := getUint16(data[2:])
	if !(vm.ac > value) {
		return nil
	}
	err := vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 3
func (vm *VirtualMachine) jgtReg(data []byte) error {
	register := data[0]
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	address := getUint16(data[1:])
	if !(vm.ac > value) {
		return nil
	}
	err = vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 4
func (vm *VirtualMachine) jleLit(data []byte) error {
	value := getUint16(data[0:])
	address := getUint16(data[2:])
	if !(vm.ac <= value) {
		return nil
	}
	err := vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 3
func (vm *VirtualMachine) jleReg(data []byte) error {
	register := data[0]
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	address := getUint16(data[1:])
	if !(vm.ac <= value) {
		return nil
	}
	err = vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 4
func (vm *VirtualMachine) jgeLit(data []byte) error {
	value := getUint16(data[0:])
	address := getUint16(data[2:])
	if !(vm.ac >= value) {
		return nil
	}
	err := vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 3
func (vm *VirtualMachine) jgeReg(data []byte) error {
	register := data[0]
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	address := getUint16(data[1:])
	if !(vm.ac >= value) {
		return nil
	}
	err = vm.jmp(address)
	if err != nil {
		return err
	}
	return nil
}

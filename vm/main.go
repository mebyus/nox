package main

import (
	"flag"
	"fmt"
	"os"
	"runtime/pprof"
	"time"

	"codeberg.org/mebyus/nox/sys"
)

func main() {
	var cpuProfile bool
	flag.BoolVar(&cpuProfile, "cpu-prof", false, "write cpu profile")
	flag.Parse()

	args := flag.Args()
	if len(args) == 0 {
		sys.Fatal("specify file with code to execute")
	}
	filename := args[0]
	code, err := os.ReadFile(filename)
	if err != nil {
		sys.Fatal(err)
	}

	if cpuProfile {
		file, err := os.Create("cpu.prof")
		if err != nil {
			sys.Fatalf("create cpu profile: %v", err)
		}
		defer file.Close()
		err = pprof.StartCPUProfile(file)
		if err != nil {
			sys.Fatalf("start cpu profile: %v", err)
		}
		defer pprof.StopCPUProfile()
	}

	start := time.Now()
	vm := NewVM()
	vm.Exec(code)
	fmt.Println(time.Since(start))
}

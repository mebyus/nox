package main

import (
	"fmt"

	"codeberg.org/mebyus/nox/vm/register"
)

type CPU struct {
	// Registers

	// instruction pointer
	ip uint16

	// (memory) stack pointer
	sp uint16

	// accumulator
	ac uint16

	// frame pointer
	fp uint16

	// General purpose registers

	r1 uint16
	r2 uint16
	r3 uint16
	r4 uint16
	r5 uint16
	r6 uint16
	r7 uint16
	r8 uint16
}

func (cpu *CPU) set(reg byte, value uint16) error {
	switch reg {
	case register.R1:
		cpu.r1 = value
	case register.R2:
		cpu.r2 = value
	case register.R3:
		cpu.r3 = value
	case register.R4:
		cpu.r4 = value
	case register.R5:
		cpu.r5 = value
	case register.R6:
		cpu.r6 = value
	case register.R7:
		cpu.r7 = value
	case register.R8:
		cpu.r8 = value
	case register.SP:
		cpu.sp = value
	case register.FP:
		cpu.fp = value
	case register.AC:
		cpu.ac = value
	default:
		return fmt.Errorf("unknown register %d", reg)
	}
	return nil
}

func (cpu *CPU) get(reg byte) (uint16, error) {
	switch reg {
	case register.R1:
		return cpu.r1, nil
	case register.R2:
		return cpu.r2, nil
	case register.R3:
		return cpu.r3, nil
	case register.R4:
		return cpu.r4, nil
	case register.R5:
		return cpu.r5, nil
	case register.R6:
		return cpu.r6, nil
	case register.R7:
		return cpu.r7, nil
	case register.R8:
		return cpu.r8, nil
	case register.SP:
		return cpu.sp, nil
	case register.FP:
		return cpu.fp, nil
	case register.AC:
		return cpu.ac, nil
	default:
		return 0, fmt.Errorf("unknown register %d", reg)
	}
}

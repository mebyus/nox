package main

import "fmt"

var (
	ErrStackOverflow = fmt.Errorf("stack overflow")
)

func (vm *VirtualMachine) psh(value uint16) error {
	if vm.sp < stackBoundary {
		return ErrStackOverflow
	}
	putUint16(vm.mem[vm.sp:], value)
	vm.sp -= 2
	return nil
}

// len(data) = 2
func (vm *VirtualMachine) pshLit(data []byte) error {
	literal := getUint16(data[0:])
	err := vm.psh(literal)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 1
func (vm *VirtualMachine) pshReg(data []byte) error {
	register := data[0]
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	err = vm.psh(value)
	if err != nil {
		return err
	}
	return nil
}

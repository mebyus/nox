package register

import "fmt"

var (
	ErrUnknownRegisterName = fmt.Errorf("unknown register name")
	ErrEmptyName           = fmt.Errorf("empty name")
)

const (
	R1 byte = 0x01
	R2 byte = 0x02
	R3 byte = 0x03
	R4 byte = 0x04
	R5 byte = 0x05
	R6 byte = 0x06
	R7 byte = 0x07
	R8 byte = 0x08
	SP byte = 0xF0
	FP byte = 0xF1
	AC byte = 0xF2
)

var ToName = [...]string{
	R1: "r1",
	R2: "r2",
	R3: "r3",
	R4: "r4",
	R5: "r5",
	R6: "r6",
	R7: "r7",
	R8: "r8",
	SP: "sp",
	FP: "fp",
	AC: "ac",
}

func FromName(name string) (register byte, err error) {
	switch name {
	case "":
		return 0, ErrEmptyName
	case "r1":
		return R1, nil
	case "r2":
		return R2, nil
	case "r3":
		return R3, nil
	case "r4":
		return R4, nil
	case "r5":
		return R5, nil
	case "r6":
		return R6, nil
	case "r7":
		return R7, nil
	case "r8":
		return R8, nil
	case "sp":
		return SP, nil
	case "fp":
		return FP, nil
	case "ac":
		return AC, nil
	default:
		return 0, ErrUnknownRegisterName
	}
}

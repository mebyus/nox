package main

// len(data) = 3
func (vm *VirtualMachine) movRegMem(data []byte) error {
	register := data[0]
	address := getUint16(data[1:])
	value, err := vm.get(register)
	if err != nil {
		return err
	}
	err = vm.memset(address, value)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 3
func (vm *VirtualMachine) movMemReg(data []byte) error {
	register := data[0]
	address := getUint16(data[1:])
	value, err := vm.memget(address)
	if err != nil {
		return err
	}
	err = vm.set(register, value)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 2
func (vm *VirtualMachine) movRegReg(data []byte) error {
	register1 := data[0]
	register2 := data[1]
	value, err := vm.get(register1)
	if err != nil {
		return err
	}
	err = vm.set(register2, value)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 3
func (vm *VirtualMachine) movLitReg(data []byte) error {
	register := data[0]
	literal := getUint16(data[1:])
	err := vm.set(register, literal)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 4
func (vm *VirtualMachine) movLitMem(data []byte) error {
	literal := getUint16(data[0:])
	address := getUint16(data[2:])
	err := vm.memset(address, literal)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 2
func (vm *VirtualMachine) movRegPtrReg(data []byte) error {
	register1 := data[0]
	register2 := data[1]
	address, err := vm.get(register1)
	if err != nil {
		return err
	}
	value, err := vm.memget(address)
	if err != nil {
		return err
	}
	err = vm.set(register2, value)
	if err != nil {
		return err
	}
	return nil
}

// len(data) = 4
func (vm *VirtualMachine) movLitOffReg(data []byte) error {
	register1 := data[0]
	register2 := data[1]
	offset := getUint16(data[2:])
	value1, err := vm.get(register1)
	if err != nil {
		return err
	}
	address := offset + value1
	value, err := vm.memget(address)
	if err != nil {
		return err
	}
	err = vm.set(register2, value)
	if err != nil {
		return err
	}
	return nil
}

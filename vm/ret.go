package main

// len(data) = 0
func (vm *VirtualMachine) ret(data []byte) error {
	vm.sp = vm.fp
	fp, err := vm.pop()
	if err != nil {
		return err
	}
	returnAddress, err := vm.pop()
	if err != nil {
		return err
	}
	nargs, err := vm.pop()
	if err != nil {
		return err
	}
	vm.fp = fp
	vm.sp -= 2 * nargs

	err = vm.jmp(returnAddress)
	if err != nil {
		return err
	}
	return nil
}

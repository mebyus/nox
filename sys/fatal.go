package sys

import (
	"fmt"
	"os"
)

func Fatal(v interface{}) {
	fmt.Println("fatal:", v)
	os.Exit(1)
}

func Fatalf(format string, v ...interface{}) {
	fmt.Printf("fatal: "+format+"\n", v...)
	os.Exit(1)
}

package main

import (
	"flag"
	"os"
	"strings"

	"codeberg.org/mebyus/nox/asn/translator"
	"codeberg.org/mebyus/nox/sys"
)

const (
	ext    = ".asn"
	outExt = ".bin"
)

func transformFilename(name string) string {
	return strings.TrimSuffix(name, ext) + outExt
}

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) == 0 {
		sys.Fatal("specify file with assembly to translate")
	}
	filename := args[0]
	code, err := translator.TranslateFile(filename)
	if err != nil {
		sys.Fatal(err)
	}
	outputFilename := transformFilename(filename)
	err = os.WriteFile(outputFilename, code, 0664)
	if err != nil {
		sys.Fatal(err)
	}
}

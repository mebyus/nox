package parser

import (
	"fmt"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/asn/token"
)

func (p *Parser) Parse() (mc *ast.ModuleCode, err error) {
	err = p.parse()
	if err != nil {
		return
	}
	mc = p.mc
	return
}

func (p *Parser) parse() (err error) {
	for {
		p.skipCommentsAndTerminators()
		if p.isEOF() {
			break
		}
		err = p.parseStatement()
		if err != nil {
			return
		}
	}
	return
}

func (p *Parser) skipCommentsAndTerminators() {
	for p.tok.Kind == token.Comment || p.tok.Kind == token.Terminator {
		p.advance()
	}
}

func (p *Parser) parseStatement() (err error) {
	switch p.tok.Kind {
	case token.Identifier:
		return p.parseLabelStatement()
	case token.Mnemonic:
		return p.parseInstructionStatement()
	default:
		return fmt.Errorf("unexpected token [ %v ] at the beginning of a statement", p.tok)
	}
	return
}

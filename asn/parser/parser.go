package parser

import (
	"io"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/asn/scanner"
	"codeberg.org/mebyus/nox/asn/token"
)

type Parser struct {
	scanner *scanner.Scanner
	mc      *ast.ModuleCode

	// token at current Parser position
	tok token.Token

	// next token
	next token.Token
}

func (p *Parser) advance() {
	p.tok = p.next
	p.next = p.scanner.Scan()
}

func (p *Parser) isEOF() bool {
	return p.tok.Kind == token.EOF
}

func FromReader(r io.Reader) (p *Parser, err error) {
	s, err := scanner.FromReader(r)
	if err != nil {
		return
	}
	p = FromScanner(s)
	return
}

func FromScanner(s *scanner.Scanner) (p *Parser) {
	p = &Parser{
		scanner: s,
		mc:      &ast.ModuleCode{},
	}

	// init Parser buffer
	p.advance()
	p.advance()
	return
}

func FromBytes(b []byte) *Parser {
	return FromScanner(scanner.FromBytes(b))
}

func FromFile(filename string) (p *Parser, err error) {
	s, err := scanner.FromFile(filename)
	if err != nil {
		return
	}
	p = FromScanner(s)
	return
}

func ParseBytes(b []byte) (mc *ast.ModuleCode, err error) {
	p := FromBytes(b)
	mc, err = p.Parse()
	return
}

func ParseFile(filename string) (mc *ast.ModuleCode, err error) {
	p, err := FromFile(filename)
	if err != nil {
		return
	}
	mc, err = p.Parse()
	return
}

func Parse(r io.Reader) (mc *ast.ModuleCode, err error) {
	p, err := FromReader(r)
	if err != nil {
		return
	}
	mc, err = p.Parse()
	return
}

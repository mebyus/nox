package parser

import (
	"fmt"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/asn/token"
)

func (p *Parser) parseLabelStatement() (err error) {
	ident := p.tok
	p.advance() // consume label name

	if p.tok.Kind != token.Colon {
		return fmt.Errorf("unexpected token [ %s ] after label", p.tok.String())
	}
	p.advance() // consume ":"

	stmt := ast.LabelStatement{
		Identifier: ident,
	}

	end, err := p.parseStatementEnd()
	if err != nil {
		return
	}
	if end {
		p.mc.Statements = append(p.mc.Statements, stmt)
		return
	}

	return fmt.Errorf("unexpected token [ %s ] at the end of label statement", p.tok.String())
}

func (p *Parser) parseInstructionStatement() (err error) {
	mnemonic := p.tok
	p.advance() // consume mnemonic

	stmt := ast.InstructionStatement{
		Mnemonic: mnemonic,
	}

	end, err := p.parseStatementEnd()
	if err != nil {
		return
	}
	if end {
		p.mc.Statements = append(p.mc.Statements, stmt)
		return
	}

	arg1, err := p.parseInstructionArgument()
	if err != nil {
		return
	}
	stmt.Argument1 = arg1

	end, err = p.parseStatementEnd()
	if err != nil {
		return
	}
	if end {
		p.mc.Statements = append(p.mc.Statements, stmt)
		return
	}

	if p.tok.Kind == token.Comma {
		p.advance() // consume ","

		arg2, err := p.parseInstructionArgument()
		if err != nil {
			return err
		}
		stmt.Argument2 = arg2

		end, err := p.parseStatementEnd()
		if err != nil {
			return err
		}
		if end {
			p.mc.Statements = append(p.mc.Statements, stmt)
			return nil
		}
	}

	return fmt.Errorf("unexpected token [ %s ] at the end of instruction statement", p.tok.String())
}

func (p *Parser) parseStatementEnd() (end bool, err error) {
	if p.tok.Kind == token.Comment {
		p.advance() // consume comment

		if p.tok.Kind == token.Terminator {
			p.advance() // consume terminator
			return true, nil
		}

		if p.isEOF() {
			return true, nil
		}

		err = fmt.Errorf("unexpected token [ %s ] at the end of instruction statement", p.tok.String())
		return false, err
	}

	if p.tok.Kind == token.Terminator {
		p.advance() // consume terminator
		return true, nil
	}

	if p.isEOF() {
		return true, nil
	}

	return false, nil
}

func (p *Parser) parseInstructionArgument() (arg ast.InstructionArgument, err error) {
	if p.tok.Kind == token.Identifier {
		ident := p.tok
		p.advance() // consume label name

		arg = ast.LabelArgument{
			Identifier: ident,
		}
		return
	}

	if p.tok.Kind == token.Register {
		reg := p.tok
		p.advance() // consume register

		arg = ast.RegisterArgument{
			Register: reg,
		}
		return
	}

	if p.tok.Kind == token.HexadecimalInteger {
		number := p.tok
		p.advance() // consume number

		arg = ast.LiteralArgument{
			Literal: number,
		}
		return
	}

	if p.tok.Kind != token.LeftSquareBracket {
		err = fmt.Errorf("unexpected token [ %s ] at the beginning of instruction argument", p.tok.String())
		return
	}
	p.advance() // consume "["

	if p.tok.Kind == token.HexadecimalInteger {
		number := p.tok
		p.advance() // consume number

		arg = ast.LiteralPointerArgument{
			Literal: number,
		}
	} else if p.tok.Kind == token.Register {
		reg := p.tok
		p.advance() // consume register

		if p.tok.Kind == token.PlusSign {
			p.advance() // consume "+"

			if p.tok.Kind != token.HexadecimalInteger {
				err = fmt.Errorf("unexpected token [ %s ] in offset pointer argument", p.tok.String())
				return
			}

			number := p.tok
			p.advance() // consume number

			arg = ast.OffsetPointerArgument{
				Register: reg,
				Offset:   number,
			}
		} else {
			arg = ast.RegisterArgument{
				Register: reg,
			}
		}
	}

	if p.tok.Kind != token.RightSquareBracket {
		err = fmt.Errorf("unexpected token [ %s ] at the end of instruction argument", p.tok.String())
		return
	}
	p.advance() // consume "]"
	return
}

package translator

import (
	"fmt"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/vm/opcode"
)

func (t *Translator) translateMov(arg1, arg2 ast.InstructionArgument) (code []byte, err error) {
	switch a1 := arg1.(type) {
	case ast.LiteralArgument:
		return t.translateMovFromLiteral(uint16(a1.Literal.Val), arg2)
	case ast.RegisterArgument:
		return t.translateMovFromRegister(byte(a1.Register.Val), arg2)
	case ast.LiteralPointerArgument:
		return t.translateMovFromLiteralPointer(uint16(a1.Literal.Val), arg2)
	case ast.RegisterPointerArgument:
	case ast.OffsetPointerArgument:
	default:
		err = fmt.Errorf("first argument cannot be of type %T", arg1)
		return
	}
	return
}

func (t *Translator) translateMovFromLiteral(lit uint16, arg2 ast.InstructionArgument) (code []byte, err error) {
	switch a2 := arg2.(type) {
	case ast.RegisterArgument:
		opc := byte(opcode.MovLitReg)
		reg := byte(a2.Register.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		putUint16(t.buf[2:], lit)
		return t.buf[:4], nil
	case ast.LiteralPointerArgument:
	case ast.RegisterPointerArgument:
	default:
		err = fmt.Errorf("second argument cannot be of type %T", arg2)
	}
	return
}

func (t *Translator) translateMovFromRegister(reg byte, arg2 ast.InstructionArgument) (code []byte, err error) {
	switch a2 := arg2.(type) {
	case ast.RegisterArgument:
		opc := byte(opcode.MovRegReg)
		reg2 := byte(a2.Register.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		t.buf[2] = reg2
		return t.buf[:3], nil
	case ast.LiteralPointerArgument:
		opc := byte(opcode.MovRegMem)
		addr := uint16(a2.Literal.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		putUint16(t.buf[2:], addr)
		return t.buf[:4], nil
	case ast.RegisterPointerArgument:
	default:
		err = fmt.Errorf("second argument cannot be of type %T", arg2)
	}
	return
}

func (t *Translator) translateMovFromLiteralPointer(addr uint16, arg2 ast.InstructionArgument) (code []byte,
	err error) {

	switch a2 := arg2.(type) {
	case ast.RegisterArgument:
		opc := byte(opcode.MovMemReg)
		reg := byte(a2.Register.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		putUint16(t.buf[2:], addr)
		return t.buf[:4], nil
	default:
		err = fmt.Errorf("second argument cannot be of type %T", arg2)
	}
	return
}

package translator

import (
	"fmt"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/vm/opcode"
)

func (t *Translator) translateJne(arg1, arg2 ast.InstructionArgument) (code []byte, err error) {
	switch a1 := arg1.(type) {
	case ast.LiteralArgument:
		return t.translateJneLiteral(uint16(a1.Literal.Val), arg2)
	case ast.RegisterArgument:
		return t.translateJneRegister(byte(a1.Register.Val), arg2)
	default:
		err = fmt.Errorf("first argument cannot be of type %T", arg1)
		return
	}
}

func (t *Translator) translateJneLiteral(lit uint16, arg2 ast.InstructionArgument) (code []byte, err error) {
	switch a2 := arg2.(type) {
	case ast.LiteralArgument:
		opc := byte(opcode.JneLit)
		addr := uint16(a2.Literal.Val)
		t.buf[0] = opc
		putUint16(t.buf[1:], lit)
		putUint16(t.buf[3:], addr)
		return t.buf[:5], nil
	case ast.LabelArgument:
		opc := byte(opcode.JneLit)
		name := a2.Identifier.Lit
		pos := a2.Identifier.Pos
		offset := uint16(len(t.code)) + 3
		addr, _ := t.resolver.Expect(name, pos, offset)
		t.buf[0] = opc
		putUint16(t.buf[1:], lit)
		putUint16(t.buf[3:], addr)
		return t.buf[:5], nil
	default:
		err = fmt.Errorf("second argument cannot be of type %T", arg2)
	}
	return
}

func (t *Translator) translateJneRegister(reg byte, arg2 ast.InstructionArgument) (code []byte, err error) {
	switch a2 := arg2.(type) {
	case ast.LiteralArgument:
		opc := byte(opcode.JneReg)
		addr := uint16(a2.Literal.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		putUint16(t.buf[2:], addr)
		return t.buf[:4], nil
	case ast.LabelArgument:
		opc := byte(opcode.JneReg)
		name := a2.Identifier.Lit
		pos := a2.Identifier.Pos
		offset := uint16(len(t.code)) + 2
		addr, _ := t.resolver.Expect(name, pos, offset)
		t.buf[0] = opc
		t.buf[1] = reg
		putUint16(t.buf[2:], addr)
		return t.buf[:4], nil
	default:
		err = fmt.Errorf("second argument cannot be of type %T", arg2)
	}
	return
}

func (t *Translator) translateJmp(arg1, arg2 ast.InstructionArgument) (code []byte, err error) {
	if arg2 != nil {
		return nil, fmt.Errorf("cannot have second argument")
	}
	switch a1 := arg1.(type) {
	case ast.LiteralArgument:
		opc := byte(opcode.Jmp)
		lit := uint16(a1.Literal.Val)
		t.buf[0] = opc
		putUint16(t.buf[1:], lit)
		return t.buf[:3], nil
	case ast.LabelArgument:
		opc := byte(opcode.Jmp)
		name := a1.Identifier.Lit
		pos := a1.Identifier.Pos
		offset := uint16(len(t.code)) + 1
		addr, _ := t.resolver.Expect(name, pos, offset)
		t.buf[0] = opc
		putUint16(t.buf[1:], addr)
		return t.buf[:3], nil
	default:
		err = fmt.Errorf("first argument cannot be of type %T", arg1)
		return
	}
}

package translator

import (
	"encoding/binary"
	"fmt"

	"codeberg.org/mebyus/nox/asn/token"
)

type LabelResolver struct {
	// maps label name to instruction offset
	resolved map[string]uint16

	// maps label name to UnknownLabel description
	unresolved map[string]UnresolvedLabel
}

type LabelPlace struct {
	// position of token which produced label argument
	Pos token.Pos

	// byte offset in code where address should be placed
	// instead of label
	Offset uint16
}

type UnresolvedLabel struct {
	Name   string
	Places []LabelPlace
}

func (r *LabelResolver) Resolve(name string, code []byte, addr uint16) error {
	_, ok := r.resolved[name]
	if ok {
		return fmt.Errorf("label \"%s\" was already declared", name)
	}
	r.resolved[name] = addr

	unresolved, ok := r.unresolved[name]
	if !ok {
		return nil
	}
	for _, place := range unresolved.Places {
		binary.LittleEndian.PutUint16(code[place.Offset:], addr)
	}
	delete(r.unresolved, name)
	return nil
}

func (r *LabelResolver) Expect(name string, pos token.Pos, offset uint16) (addr uint16, ok bool) {
	addr, ok = r.resolved[name]
	if ok {
		return addr, true
	}
	unresolved := r.unresolved[name]
	unresolved.Places = append(unresolved.Places, LabelPlace{
		Pos:    pos,
		Offset: offset,
	})
	r.unresolved[name] = unresolved
	return 0, false
}

func (r *LabelResolver) ListUnresolved() []UnresolvedLabel {
	if len(r.unresolved) == 0 {
		return nil
	}

	list := make([]UnresolvedLabel, 0, len(r.unresolved))
	for _, unresolved := range r.unresolved {
		list = append(list, unresolved)
	}
	return list
}

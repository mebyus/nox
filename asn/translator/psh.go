package translator

import (
	"fmt"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/vm/opcode"
)

func (t *Translator) translatePsh(arg1, arg2 ast.InstructionArgument) (code []byte, err error) {
	if arg2 != nil {
		return nil, fmt.Errorf("cannot have second argument")
	}
	switch a1 := arg1.(type) {
	case ast.LiteralArgument:
		opc := byte(opcode.PshLit)
		lit := uint16(a1.Literal.Val)
		t.buf[0] = opc
		putUint16(t.buf[1:], lit)
		return t.buf[:3], nil
	case ast.RegisterArgument:
		opc := byte(opcode.PshReg)
		reg := byte(a1.Register.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		return t.buf[:2], nil
	default:
		err = fmt.Errorf("first argument cannot be of type %T", arg1)
		return
	}
}

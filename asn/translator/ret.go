package translator

import (
	"fmt"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/vm/opcode"
)

func (t *Translator) translateRet(arg1, arg2 ast.InstructionArgument) (code []byte, err error) {
	if arg1 != nil || arg2 != nil {
		return nil, fmt.Errorf("cannot have arguments")
	}
	opc := byte(opcode.Ret)
	t.buf[0] = opc
	return t.buf[:1], nil
}

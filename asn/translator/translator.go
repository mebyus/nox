package translator

import (
	"fmt"
	"os"
	"time"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/asn/parser"
)

type Translator struct {
	code     []byte
	resolver LabelResolver

	// current instruction buffer
	buf [5]byte

	mc *ast.ModuleCode
}

func FromAST(mc *ast.ModuleCode) *Translator {
	return &Translator{
		mc: mc,
		resolver: LabelResolver{
			resolved:   make(map[string]uint16),
			unresolved: make(map[string]UnresolvedLabel),
		},
	}
}

func TranslateFile(filename string) (code []byte, err error) {
	b, err := os.ReadFile(filename)
	if err != nil {
		return
	}
	start := time.Now()
	mc, err := parser.ParseBytes(b)
	if err != nil {
		return
	}
	t := FromAST(mc)
	err = t.Translate()
	if err != nil {
		return
	}
	fmt.Println(time.Since(start))
	code = t.code
	return
}

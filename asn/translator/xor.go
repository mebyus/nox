package translator

import (
	"fmt"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/vm/opcode"
)

func (t *Translator) translateXor(arg1, arg2 ast.InstructionArgument) (code []byte, err error) {
	switch a1 := arg1.(type) {
	case ast.LiteralArgument:
		return t.translateOrWithLiteral(uint16(a1.Literal.Val), arg2)
	case ast.RegisterArgument:
		return t.translateOrWithRegister(byte(a1.Register.Val), arg2)
	default:
		err = fmt.Errorf("first argument cannot be of type %T", arg1)
		return
	}
}

func (t *Translator) translateXorWithLiteral(lit uint16, arg2 ast.InstructionArgument) (code []byte, err error) {
	switch a2 := arg2.(type) {
	case ast.RegisterArgument:
		opc := byte(opcode.XorRegLit)
		reg := byte(a2.Register.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		putUint16(t.buf[2:], lit)
		return t.buf[:4], nil
	default:
		err = fmt.Errorf("second argument cannot be of type %T", arg2)
	}
	return
}

func (t *Translator) translateXorWithRegister(reg byte, arg2 ast.InstructionArgument) (code []byte, err error) {
	switch a2 := arg2.(type) {
	case ast.RegisterArgument:
		opc := byte(opcode.XorRegReg)
		reg2 := byte(a2.Register.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		t.buf[2] = reg2
		return t.buf[:3], nil
	case ast.LiteralArgument:
		opc := byte(opcode.XorRegLit)
		lit := uint16(a2.Literal.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		putUint16(t.buf[2:], lit)
		return t.buf[:4], nil
	default:
		err = fmt.Errorf("second argument cannot be of type %T", arg2)
	}
	return
}

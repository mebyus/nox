package translator

import (
	"fmt"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/vm/opcode"
)

func (t *Translator) translateInc(arg1, arg2 ast.InstructionArgument) (code []byte, err error) {
	if arg2 != nil {
		return nil, fmt.Errorf("cannot have second argument")
	}
	switch a1 := arg1.(type) {
	case ast.RegisterArgument:
		opc := byte(opcode.Inc)
		reg := byte(a1.Register.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		return t.buf[:2], nil
	default:
		err = fmt.Errorf("first argument cannot be of type %T", arg1)
		return
	}
}

func (t *Translator) translateDec(arg1, arg2 ast.InstructionArgument) (code []byte, err error) {
	if arg2 != nil {
		return nil, fmt.Errorf("cannot have second argument")
	}
	switch a1 := arg1.(type) {
	case ast.RegisterArgument:
		opc := byte(opcode.Dec)
		reg := byte(a1.Register.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		return t.buf[:2], nil
	default:
		err = fmt.Errorf("first argument cannot be of type %T", arg1)
		return
	}
}

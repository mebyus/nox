package translator

import (
	"encoding/binary"
	"fmt"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/vm/mnemo"
)

func (t *Translator) Translate() (err error) {
	// preallocate approximate space needed for translated instructions
	t.code = make([]byte, 0, 4*len(t.mc.Statements))
	for _, stmt := range t.mc.Statements {
		err = t.translateStatement(stmt)
		if err != nil {
			return
		}
	}
	unresolved := t.resolver.ListUnresolved()
	if len(unresolved) > 0 {
		for _, un := range unresolved {
			fmt.Println(un.Name, un.Places)
		}
		fmt.Println()

		err = fmt.Errorf("code contains unresolved labels")
		return
	}
	return nil
}

func (t *Translator) translateStatement(stmt ast.Statement) (err error) {
	switch s := stmt.(type) {
	case ast.LabelStatement:
		err = t.translateLabelStatement(s)
	case ast.InstructionStatement:
		err = t.translateInstructionStatement(s)
	default:
		panic(fmt.Sprintf("unexpected statement type %T", stmt))
	}
	return
}

func (t *Translator) translateLabelStatement(stmt ast.LabelStatement) (err error) {
	name := stmt.Identifier.Lit
	err = t.resolver.Resolve(name, t.code, uint16(len(t.code)))
	return
}

func (t *Translator) translateInstructionStatement(stmt ast.InstructionStatement) (err error) {
	mnemonicCode := mnemo.Code(stmt.Mnemonic.Val)
	arg1 := stmt.Argument1
	arg2 := stmt.Argument2

	var code []byte
	switch mnemonicCode {
	case mnemo.CodeNop:
		code, err = t.translateNop(arg1, arg2)
	case mnemo.CodeMov:
		code, err = t.translateMov(arg1, arg2)
	case mnemo.CodeAdd:
		code, err = t.translateAdd(arg1, arg2)
	case mnemo.CodeSub:
		code, err = t.translateSub(arg1, arg2)
	case mnemo.CodeInc:
		code, err = t.translateInc(arg1, arg2)
	case mnemo.CodeDec:
		code, err = t.translateDec(arg1, arg2)
	case mnemo.CodeLsh:
		code, err = t.translateLsh(arg1, arg2)
	case mnemo.CodeRsh:
		code, err = t.translateRsh(arg1, arg2)
	case mnemo.CodeAnd:
		code, err = t.translateAnd(arg1, arg2)
	case mnemo.CodeOr:
		code, err = t.translateOr(arg1, arg2)
	case mnemo.CodeXor:
		code, err = t.translateXor(arg1, arg2)
	case mnemo.CodeNot:
		code, err = t.translateNot(arg1, arg2)
	case mnemo.CodeJne:
		code, err = t.translateJne(arg1, arg2)
	case mnemo.CodeJeq:
	case mnemo.CodeJlt:
	case mnemo.CodeJgt:
	case mnemo.CodeJle:
	case mnemo.CodeJge:
	case mnemo.CodeJmp:
		code, err = t.translateJmp(arg1, arg2)
	case mnemo.CodePsh:
		code, err = t.translatePsh(arg1, arg2)
	case mnemo.CodePop:
		code, err = t.translatePop(arg1, arg2)
	case mnemo.CodeCal:
		code, err = t.translateCal(arg1, arg2)
	case mnemo.CodeRet:
		code, err = t.translateRet(arg1, arg2)
	case mnemo.CodeHlt:
		code, err = t.translateHlt(arg1, arg2)
	default:
		panic(fmt.Sprintf("unknown mnemonic code %d", mnemonicCode))
	}
	if err != nil {
		return fmt.Errorf("%v translate %s instruction: %v", stmt.Mnemonic.Pos, mnemo.ToMnemo[mnemonicCode], err)
	}

	if len(t.code)+len(code) >= 1<<16 {
		return fmt.Errorf("64kb code limit exceeded")
	}

	t.code = append(t.code, code...)
	return
}

func putUint16(b []byte, v uint16) {
	binary.LittleEndian.PutUint16(b, v)
}

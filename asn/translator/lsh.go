package translator

import (
	"fmt"

	"codeberg.org/mebyus/nox/asn/ast"
	"codeberg.org/mebyus/nox/vm/opcode"
)

func (t *Translator) translateLsh(arg1, arg2 ast.InstructionArgument) (code []byte, err error) {
	var reg byte // shifted register
	switch a1 := arg1.(type) {
	case ast.RegisterArgument:
		reg = byte(a1.Register.Val)
	default:
		err = fmt.Errorf("first argument cannot be of type %T", arg1)
		return
	}

	switch a2 := arg2.(type) {
	case ast.LiteralArgument:
		opc := byte(opcode.LshRegLit)
		lit := uint16(a2.Literal.Val)
		t.buf[0] = opc
		t.buf[1] = reg
		putUint16(t.buf[2:], lit)
		return t.buf[:4], nil
	case ast.RegisterArgument:
		opc := byte(opcode.LshRegReg)
		reg2 := byte(a2.Register.Val) // register with shift value
		t.buf[0] = opc
		t.buf[1] = reg
		t.buf[2] = reg2
		return t.buf[:3], nil
	default:
		err = fmt.Errorf("second argument cannot be of type %T", arg2)
		return
	}
}

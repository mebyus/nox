package token

import (
	"fmt"

	"codeberg.org/mebyus/nox/vm/mnemo"
	"codeberg.org/mebyus/nox/vm/register"
)

type Kind int

type Token struct {
	Kind Kind
	Pos  Pos

	// Not empty only for tokens which can have
	// arbitrary literal
	//
	// Examples: identifiers or numbers
	Lit string

	// Meaning depends on Token.Kind
	//
	// 1. Number value for tokens which represent numbers
	//
	// 2. Mnemonic code for tokens which represent mnemonics
	//
	// 3. Register number for tokens which represent registers
	Val int
}

const (
	EOF Kind = iota
	Terminator
	Illegal
	Comment
	Identifier
	Mnemonic
	Register
	BinaryInteger
	OctalInteger
	DecimalInteger
	HexadecimalInteger
	PlusSign           // +
	Comma              // ,
	Colon              // :
	LeftSquareBracket  // [
	RightSquareBracket // ]

	Import
	Module
)

var Literal = [...]string{
	// Non static or empty literals
	EOF:                "EOF",
	Terminator:         "TERM",
	Illegal:            "ILLEGAL",
	Comment:            "COMMENT",
	Identifier:         "IDENT",
	Mnemonic:           "MNEMO",
	Register:           "REGISTER",
	BinaryInteger:      "BININT",
	OctalInteger:       "OCTINT",
	DecimalInteger:     "DECINT",
	HexadecimalInteger: "HEXINT",

	// Operators/punctuators
	PlusSign:           "+",
	Colon:              ":",
	Comma:              ",",
	LeftSquareBracket:  "[",
	RightSquareBracket: "]",

	// Keywords
	Import: "import",
	Module: "module",
}

var Keyword = map[string]Kind{
	"import": Import,
	"module": Module,
}

func (tok *Token) String() string {
	if tok.Kind >= Import {
		return fmt.Sprintf("%-12s%s", tok.Pos.String(), Literal[tok.Kind])
	}
	if tok.Kind == Mnemonic {
		return fmt.Sprintf("%-12s%-12s%s", tok.Pos.String(), Literal[tok.Kind], mnemo.ToMnemo[tok.Val])
	}
	if tok.Kind == Register {
		return fmt.Sprintf("%-12s%-12s%s", tok.Pos.String(), Literal[tok.Kind], register.ToName[tok.Val])
	}
	return fmt.Sprintf("%-12s%-12s%s", tok.Pos.String(), Literal[tok.Kind], tok.Lit)
}

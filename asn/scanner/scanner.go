package scanner

import (
	"io"
	"os"

	"codeberg.org/mebyus/nox/asn/token"
)

type Scanner struct {
	// b's read index
	i int

	// b's mark index
	m int

	// character code at current Scanner position
	c int

	// next character code
	next int

	// source text which is scanned by the Scanner
	b []byte

	// Scanner position inside source text
	pos token.Pos
}

func FromBytes(b []byte) (s *Scanner) {
	s = &Scanner{b: b}

	// init Scanner buffer
	for i := 0; i < bufsize; i++ {
		s.advance()
	}

	s.pos = token.Pos{}
	return
}

func FromFile(filename string) (s *Scanner, err error) {
	b, err := os.ReadFile(filename)
	if err != nil {
		return
	}
	s = FromBytes(b)
	return
}

func FromReader(r io.Reader) (s *Scanner, err error) {
	b, err := io.ReadAll(r)
	if err != nil {
		return
	}
	s = FromBytes(b)
	return
}

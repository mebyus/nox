package scanner

import (
	"strconv"

	"codeberg.org/mebyus/nox/asn/token"
	"codeberg.org/mebyus/nox/vm/mnemo"
	"codeberg.org/mebyus/nox/vm/register"
)

func (s *Scanner) Scan() token.Token {
	if s.c == eof {
		return s.createToken(token.EOF)
	}

	s.skipWhitespace()
	if s.c == eof {
		return s.createToken(token.EOF)
	}

	if isLetterOrUnderscore(s.c) {
		return s.scanName()
	}

	if isDecimalDigit(s.c) {
		return s.scanNumber()
	}

	if s.c == ';' || (s.c == '/' && s.next == '/') {
		return s.scanLineComment()
	}

	return s.scanOther()
}

func (s *Scanner) createToken(kind token.Kind) token.Token {
	return token.Token{
		Kind: kind,
		Pos:  s.pos,
	}
}

func (s *Scanner) scanName() (tok token.Token) {
	tok.Pos = s.pos
	s.mark()

	s.consumeWord()
	lit := s.slice()

	code, ok := mnemo.ToCode[lit]
	if ok {
		tok.Kind = token.Mnemonic
		tok.Val = code
		return
	}
	reg, err := register.FromName(lit)
	if err == nil {
		tok.Kind = token.Register
		tok.Val = int(reg)
		return
	}
	tok.Kind = token.Identifier
	tok.Lit = lit
	return
}

func (s *Scanner) scanBinaryNumber() (tok token.Token) {
	tok.Pos = s.pos
	s.mark()

	s.advance() // consume '0' byte
	s.advance() // consume 'b' byte

	scannedAtLeastOneDigit := false
	for s.c != eof && isBinaryDigit(s.c) {
		s.advance()
		scannedAtLeastOneDigit = true
	}

	if isAlphanum(s.c) {
		s.consumeWord()
		tok.Kind = token.Illegal
		tok.Lit = s.slice()
		return
	}

	if !scannedAtLeastOneDigit {
		tok.Kind = token.Illegal
		tok.Lit = s.slice()
		return
	}

	tok.Kind = token.BinaryInteger
	tok.Lit = s.slice()
	return
}

func (s *Scanner) scanOctalNumber() (tok token.Token) {
	tok.Pos = s.pos
	s.mark()

	s.advance() // consume '0' byte
	s.advance() // consume 'o' byte

	scannedAtLeastOneDigit := false
	for s.c != eof && isOctalDigit(s.c) {
		s.advance()
		scannedAtLeastOneDigit = true
	}

	if isAlphanum(s.c) {
		s.consumeWord()
		tok.Kind = token.Illegal
		tok.Lit = s.slice()
		return
	}

	if !scannedAtLeastOneDigit {
		tok.Kind = token.Illegal
		tok.Lit = s.slice()
		return
	}

	tok.Kind = token.OctalInteger
	tok.Lit = s.slice()
	return
}

func (s *Scanner) scanDecimalNumber() (tok token.Token) {
	tok.Pos = s.pos
	s.mark()

	for s.c != eof && isDecimalDigit(s.c) {
		s.advance()
	}

	if isAlphanum(s.c) || s.c == '.' {
		s.consumeWord()
		tok.Kind = token.Illegal
		tok.Lit = s.slice()
		return
	}

	tok.Kind = token.DecimalInteger
	tok.Lit = s.slice()
	return
}

func (s *Scanner) scanHexadecimalNumber() (tok token.Token) {
	tok.Pos = s.pos
	s.mark()

	s.advance() // consume '0' byte
	s.advance() // consume 'x' byte

	scannedAtLeastOneDigit := false
	for s.c != eof && isHexadecimalDigit(s.c) {
		s.advance()
		scannedAtLeastOneDigit = true
	}

	if isAlphanum(s.c) {
		s.consumeWord()
		tok.Kind = token.Illegal
		tok.Lit = s.slice()
		return
	}

	if !scannedAtLeastOneDigit {
		tok.Kind = token.Illegal
		tok.Lit = s.slice()
		return
	}

	lit := s.slice()
	number, err := strconv.ParseUint(lit[2:], 16, 16)
	if err != nil || number >= 1<<16 {
		tok.Kind = token.Illegal
		tok.Lit = lit
		return
	}

	tok.Kind = token.HexadecimalInteger
	tok.Lit = lit
	tok.Val = int(number)
	return
}

func (s *Scanner) scanNumber() (tok token.Token) {
	if s.c != '0' {
		return s.scanDecimalNumber()
	}

	if s.next == eof {
		tok = token.Token{
			Kind: token.DecimalInteger,
			Pos:  s.pos,
			Lit:  stringFromByte('0'),
		}
		s.advance()
		return
	}

	if s.next == 'b' {
		return s.scanBinaryNumber()
	}

	if s.next == 'o' {
		return s.scanOctalNumber()
	}

	if s.next == 'x' {
		return s.scanHexadecimalNumber()
	}

	if isAlphanum(s.next) {
		return s.scanIllegalWord()
	}

	tok = token.Token{
		Kind: token.DecimalInteger,
		Pos:  s.pos,
		Lit:  stringFromByte('0'),
	}
	s.advance()
	return
}

func (s *Scanner) scanLineComment() (tok token.Token) {
	tok.Kind = token.Comment
	tok.Pos = s.pos
	s.mark()

	for s.c != eof && s.c != '\n' {
		s.advance()
	}

	tok.Lit = s.slice()
	// if s.c != '\n' {
	// 	return
	// }
	// s.advance()
	return
}

func (s *Scanner) scanSingleByteToken(kind token.Kind) token.Token {
	tok := s.createToken(kind)
	s.advance()
	return tok
}

func (s *Scanner) scanIllegalWord() (tok token.Token) {
	tok.Kind = token.Illegal
	tok.Pos = s.pos
	s.mark()
	s.consumeWord()
	tok.Lit = s.slice()
	return
}

func (s *Scanner) scanIllegalByteToken() token.Token {
	tok := token.Token{
		Kind: token.Illegal,
		Pos:  s.pos,
		Lit:  stringFromByte(byte(s.c)),
	}
	s.advance()
	return tok
}

func (s *Scanner) scanOther() token.Token {
	switch s.c {
	case '[':
		return s.scanSingleByteToken(token.LeftSquareBracket)
	case ']':
		return s.scanSingleByteToken(token.RightSquareBracket)
	case '+':
		return s.scanSingleByteToken(token.PlusSign)
	case ',':
		return s.scanSingleByteToken(token.Comma)
	case ':':
		return s.scanSingleByteToken(token.Colon)
	case '\n':
		return s.scanSingleByteToken(token.Terminator)
	default:
		return s.scanIllegalByteToken()
	}
}

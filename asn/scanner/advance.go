package scanner

const eof = -1
const bufsize = 2

func (s *Scanner) advance() {
	if s.c != eof {
		if s.c == '\n' {
			s.pos.NextLine()
		} else {
			s.pos.NextCol()
		}
	}
	s.c = s.next
	if s.i < len(s.b) {
		s.next = int(s.b[s.i])
	} else {
		s.next = eof
	}
	s.i++
}

func (s *Scanner) skipWhitespace() {
	for isWhitespace(s.c) {
		s.advance()
	}
}

func (s *Scanner) skipSpace() {
	for s.c == ' ' {
		s.advance()
	}
}

func (s *Scanner) consumeWord() {
	for isAlphanum(s.c) {
		s.advance()
	}
}

func (s *Scanner) mark() {
	s.m = s.i - bufsize
}

func (s *Scanner) slice() string {
	return string(s.b[s.m : s.i-bufsize])
}

package ast

import "codeberg.org/mebyus/nox/asn/token"

// <ModuleCode> = { <Statement> }
type ModuleCode struct {
	Statements []Statement
}

// <Statement> = <LabelStatement> | <InstructionStatement>
type Statement any

// <LabelStatement> = <Identifier> ":" <TERM>
type LabelStatement struct {
	Identifier token.Token
}

// <InstructionStatement> = <Mnemonic> [ <InstructionArgument> [ "," <InstructionArgument> ] ] <TERM>
type InstructionStatement struct {
	Mnemonic  token.Token
	Argument1 InstructionArgument
	Argument2 InstructionArgument
}

// <InstructionArgument> = <LiteralArgument> | <RegisterArgument> | <LiteralPointerArgument> |
//
//	| <RegisterPointerArgument> | <OffsetPointerArgument> | <LabelArgument>
type InstructionArgument any

// <LiteralArgument> = <IntegerNumber>
type LiteralArgument struct {
	Literal token.Token
}

// <RegisterArgument> = <RegisterIdentifier>
type RegisterArgument struct {
	Register token.Token
}

// <LiteralPointerArgument> = "[" <IntegerNumber> "]"
type LiteralPointerArgument struct {
	Literal token.Token
}

// <RegisterPointerArgument> = "[" <RegisterIdentifier> "]"
type RegisterPointerArgument struct {
	Register token.Token
}

// <OffsetPointerArgument> = "[" <RegisterIdentifier> "+" <IntegerNumber> "]"
type OffsetPointerArgument struct {
	Offset   token.Token
	Register token.Token
}

// <LabelArgument> = <Identifier>
type LabelArgument struct {
	Identifier token.Token
}

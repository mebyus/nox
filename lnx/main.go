package main

import (
	"flag"
	"os"

	"codeberg.org/mebyus/nox/sys"
)

func main() {
	var outputFilename string
	flag.StringVar(&outputFilename, "o", "", "output filename")
	flag.Parse()

	args := flag.Args()
	if len(args) == 0 {
		sys.Fatal("specify .onx file(s) to link")
	}
	if outputFilename == "" {
		sys.Fatal("specify output filename via -o flag")
	}
	filenames := args
	code, err := LinkObjectFiles(filenames...)
	if err != nil {
		sys.Fatal(err)
	}
	err = os.WriteFile(outputFilename, code, 0664)
	if err != nil {
		sys.Fatal(err)
	}
}

func LinkObjectFiles(names ...string) (code []byte, err error) {
	return
}

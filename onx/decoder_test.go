package onx

import (
	"bytes"
	"reflect"
	"testing"
)

func TestEncodeDecode(t *testing.T) {
	tests := []struct {
		name   string
		module *Module
	}{
		{
			name:   "1. Empty module",
			module: &Module{},
		},
		{
			name: "2. Only module name",
			module: &Module{
				Header: ModuleHeader{
					Name: "my_module",
				},
			},
		},
		{
			name: "3. Imported module",
			module: &Module{
				Header: ModuleHeader{
					Name: "my_module",
				},
				ImportSection: ImportSection{
					Modules: []ImportedModule{
						{
							Name: "other_module",
						},
					},
				},
			},
		},
		{
			name: "3. Imported module with consts",
			module: &Module{
				Header: ModuleHeader{
					Name: "my_module",
				},
				ImportSection: ImportSection{
					Modules: []ImportedModule{
						{
							Name: "other_module",
							Consts: []ImportedConst{
								{
									Name:   "procs",
									Usages: []uint16{0x0054, 0x0098},
								},
								{
									Name:   "apple",
									Usages: []uint16{0x0004, 0x1018, 0xA4A8},
								},
							},
						},
						{
							Name: "other_module2",
							Consts: []ImportedConst{
								{
									Name:   "procs2",
									Usages: []uint16{0x0054},
								},
								{
									Name:   "apple2",
									Usages: []uint16{0x0004, 0x0000, 0xA4A8},
								},
							},
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buf := bytes.Buffer{}
			err := NewEncoder(&buf).Encode(tt.module)
			if err != nil {
				t.Errorf("Encode() error = %v", err)
				return
			}
			gotModule, err := Decode(&buf)
			if err != nil {
				t.Errorf("Decode() error = %v", err)
				return
			}
			tt.module.Header.Offset = Offset{}
			if !reflect.DeepEqual(gotModule, tt.module) {
				t.Errorf("Decode() = %v, want %v", gotModule, tt.module)
			}
		})
	}
}

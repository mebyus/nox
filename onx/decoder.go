package onx

import (
	"fmt"
	"io"
)

type Decoder struct {
	reader io.Reader

	buf []byte
	pos uint16
}

func NewDecoder(reader io.Reader) *Decoder {
	return &Decoder{
		reader: reader,
	}
}

func (d *Decoder) Decode() (module *Module, err error) {
	d.buf, err = io.ReadAll(d.reader)
	if err != nil {
		return
	}
	if len(d.buf) >= 1<<16 {
		return nil, fmt.Errorf("module encoded size exceeds 64kb")
	}
	module = &Module{}
	err = d.decodeModule(module)
	return
}

func Decode(reader io.Reader) (module *Module, err error) {
	decoder := NewDecoder(reader)
	return decoder.Decode()
}

package onx

import "encoding/binary"

func putUint16(b []byte, v uint16) {
	binary.LittleEndian.PutUint16(b, v)
}

func (e *Encoder) encodeUint16(v uint16) {
	putUint16(e.buf[e.pos:], v)
	e.pos += 2
}

func (e *Encoder) encodeString(s string) {
	e.encodeUint16(uint16(len(s)))
	copy(e.buf[e.pos:], s)
	e.pos += uint16(len(s))
}

func (e *Encoder) encodeBytes(b []byte) {
	copy(e.buf[e.pos:], b)
	e.pos += uint16(len(b))
}

func (e *Encoder) encodeOffset(offset Offset) {
	e.encodeUint16(offset.ImportSection)
	e.encodeUint16(offset.LocalSection)
	e.encodeUint16(offset.CodeSection)
}

func (e *Encoder) encodeHeader(header ModuleHeader) {
	e.encodeOffset(header.Offset)
	e.encodeString(header.Name)
}

func (e *Encoder) encodeImportSection(section ImportSection) {
	modules := section.Modules
	e.encodeUint16(uint16(len(modules)))
	for _, module := range modules {
		e.encodeImportedModule(module)
	}
}

func (e *Encoder) encodeUint16Array(slice []uint16) {
	e.encodeUint16(uint16(len(slice)))
	for _, elem := range slice {
		e.encodeUint16(elem)
	}
}

func (e *Encoder) encodeImportedConst(c ImportedConst) {
	e.encodeString(c.Name)
	e.encodeUint16Array(c.Usages)
}

func (e *Encoder) encodeImportedVar(v ImportedVar) {
	e.encodeString(v.Name)
	e.encodeUint16Array(v.Usages)
}

func (e *Encoder) encodeImportedFunction(f ImportedFunction) {
	e.encodeString(f.Name)
	e.encodeUint16Array(f.Usages)
}

func (e *Encoder) encodeImportedModule(module ImportedModule) {
	e.encodeString(module.Name)

	e.encodeUint16(uint16(len(module.Consts)))
	for _, c := range module.Consts {
		e.encodeImportedConst(c)
	}

	e.encodeUint16(uint16(len(module.Vars)))
	for _, v := range module.Vars {
		e.encodeImportedVar(v)
	}

	e.encodeUint16(uint16(len(module.Funcs)))
	for _, f := range module.Funcs {
		e.encodeImportedFunction(f)
	}
}

func (e *Encoder) encodeLocalSection(section LocalSection) {
	e.encodeUint16(uint16(len(section.Consts)))
	for _, c := range section.Consts {
		e.encodeString(c.Name)
		e.encodeUint16(c.Value)
	}

	e.encodeUint16(uint16(len(section.Vars)))
	for _, v := range section.Vars {
		e.encodeString(v.Name)
		e.encodeUint16(v.Value)
		e.encodeUint16Array(v.Usages)
	}

	e.encodeUint16(uint16(len(section.Funcs)))
	for _, f := range section.Funcs {
		e.encodeString(f.Name)
		e.encodeUint16(f.Origin)
		e.encodeUint16Array(f.Usages)
	}
}

func (e *Encoder) encodeCodeSection(code []byte) {
	e.encodeBytes(code)
}

func (e *Encoder) encodeModule(m *Module) {
	e.encodeHeader(m.Header)
	e.encodeImportSection(m.ImportSection)
	e.encodeLocalSection(m.LocalSection)
	e.encodeCodeSection(m.CodeSection)
}

package onx

type Module struct {
	Header        ModuleHeader
	ImportSection ImportSection
	LocalSection  LocalSection
	CodeSection   []byte
}

type Offset struct {
	ImportSection uint16
	LocalSection  uint16
	CodeSection   uint16
}

type ModuleHeader struct {
	Offset Offset

	Name string
}

type ImportSection struct {
	Modules []ImportedModule
}

type ImportedModule struct {
	Name string

	Consts []ImportedConst
	Vars   []ImportedVar
	Funcs  []ImportedFunction
}

type ImportedConst struct {
	Name string

	Usages []uint16
}

type ImportedVar struct {
	Name string

	Usages []uint16
}

type ImportedFunction struct {
	Name string

	Usages []uint16
}

type LocalSection struct {
	Consts []LocalConst
	Vars   []LocalVar
	Funcs  []LocalFunction
}

type LocalConst struct {
	Name  string
	Value uint16
}

type LocalVar struct {
	Name   string
	Value  uint16
	Usages []uint16
}

type LocalFunction struct {
	Name   string
	Origin uint16
	Usages []uint16
}

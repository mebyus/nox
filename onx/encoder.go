package onx

import (
	"fmt"
	"io"
)

type Encoder struct {
	writer io.Writer

	buf []byte
	pos uint16
}

func NewEncoder(writer io.Writer) *Encoder {
	return &Encoder{
		writer: writer,
	}
}

func (e *Encoder) Encode(m *Module) error {
	size, offset := m.Len()
	if size >= 1<<16 {
		return fmt.Errorf("module encoded size exceeds 64kb")
	}
	m.Header.Offset = offset
	e.buf = make([]byte, size)
	e.encodeModule(m)
	_, err := e.writer.Write(e.buf)
	return err
}

func Encode(writer io.Writer, module *Module) error {
	encoder := NewEncoder(writer)
	return encoder.Encode(module)
}

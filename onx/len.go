package onx

func (m *Module) Len() (n int, offset Offset) {
	headerLen := m.Header.Len()
	n += headerLen

	offset.ImportSection = uint16(n)
	importSectionLen := m.ImportSection.Len()
	n += importSectionLen

	offset.LocalSection = uint16(n)
	localSectionLen := m.LocalSection.Len()
	n += localSectionLen

	offset.CodeSection = uint16(n)
	n += len(m.CodeSection)
	return
}

func (off Offset) Len() int {
	return 2 + 2 + 2
}

func (mh *ModuleHeader) Len() int {
	return mh.Offset.Len() + 2 + len(mh.Name)
}

func (ic ImportedConst) Len() int {
	return 2 + len(ic.Name) + 2 + 2*len(ic.Usages)
}

func (iv ImportedVar) Len() int {
	return 2 + len(iv.Name) + 2 + 2*len(iv.Usages)
}

func (ifn ImportedFunction) Len() int {
	return 2 + len(ifn.Name) + 2 + 2*len(ifn.Usages)
}

func (im *ImportedModule) Len() int {
	n := 2 + len(im.Name)

	n += 2
	for _, ic := range im.Consts {
		n += ic.Len()
	}

	n += 2
	for _, iv := range im.Vars {
		n += iv.Len()
	}

	n += 2
	for _, ifn := range im.Funcs {
		n += ifn.Len()
	}

	return n
}

func (is ImportSection) Len() int {
	n := 2
	for _, im := range is.Modules {
		n += im.Len()
	}

	return n
}

func (lc LocalConst) Len() int {
	return 2 + len(lc.Name) + 2
}

func (lv LocalVar) Len() int {
	return 2 + len(lv.Name) + 2 + 2 + len(lv.Usages)
}

func (lf LocalFunction) Len() int {
	return 2 + len(lf.Name) + 2 + 2 + len(lf.Usages)
}

func (ls *LocalSection) Len() int {
	n := 2
	for _, lc := range ls.Consts {
		n += lc.Len()
	}

	n += 2
	for _, lv := range ls.Vars {
		n += lv.Len()
	}

	n += 2
	for _, lf := range ls.Funcs {
		n += lf.Len()
	}
	return n
}

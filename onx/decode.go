package onx

import (
	"encoding/binary"
	"fmt"
)

func getUint16(b []byte) uint16 {
	return binary.LittleEndian.Uint16(b)
}

func (d *Decoder) bytesLeft() int {
	return len(d.buf) - int(d.pos)
}

func (d *Decoder) decodeString() (s string, err error) {
	n, err := d.decodeUint16()
	if err != nil {
		return "", fmt.Errorf("string length: %w", err)
	}
	if d.bytesLeft() < int(n) {
		return "", fmt.Errorf("not enough bytes to decode string")
	}
	s = string(d.buf[d.pos : d.pos+n])
	d.pos += n
	return
}

func (d *Decoder) decodeUint16() (v uint16, err error) {
	if d.bytesLeft() < 2 {
		return 0, fmt.Errorf("not enough bytes to decode uint16 value")
	}
	v = getUint16(d.buf[d.pos:])
	d.pos += 2
	return
}

func (d *Decoder) decodeUint16Array() (s []uint16, err error) {
	n, err := d.decodeUint16()
	if err != nil {
		return nil, fmt.Errorf("array length: %w", err)
	}
	if d.bytesLeft() < 2*int(n) {
		return nil, fmt.Errorf("not enough bytes to decode uint16 array")
	}
	for i := uint16(0); i < n; i++ {
		s = append(s, getUint16(d.buf[d.pos:]))
		d.pos += 2
	}
	return s, nil
}

func (d *Decoder) decodeModule(module *Module) (err error) {
	err = d.decodeHeader(&module.Header)
	if err != nil {
		return fmt.Errorf("decode header: %w", err)
	}
	err = d.decodeImportSection(&module.ImportSection)
	if err != nil {
		return fmt.Errorf("decode import section: %w", err)
	}
	err = d.decodeLocalSection(&module.LocalSection)
	if err != nil {
		return fmt.Errorf("decode local section: %w", err)
	}
	module.CodeSection = d.decodeCodeSection()
	return
}

func (d *Decoder) decodeHeader(header *ModuleHeader) (err error) {
	d.pos += 6 // offsets numbers

	name, err := d.decodeString()
	if err != nil {
		return
	}
	header.Name = name
	return
}

func (d *Decoder) decodeImportSection(section *ImportSection) (err error) {
	n, err := d.decodeUint16()
	if err != nil {
		return fmt.Errorf("number of modules: %w", err)
	}
	for i := uint16(0); i < n; i++ {
		module, err := d.decodeImportedModule()
		if err != nil {
			return fmt.Errorf("decode module %d: %w", i+1, err)
		}
		section.Modules = append(section.Modules, module)
	}
	return nil
}

func (d *Decoder) decodeLocalSection(section *LocalSection) (err error) {
	consts, err := d.decodeLocalConsts()
	if err != nil {
		return
	}
	vars, err := d.decodeLocalVars()
	if err != nil {
		return
	}
	funcs, err := d.decodeLocalFunctions()
	if err != nil {
		return
	}
	section.Consts = consts
	section.Vars = vars
	section.Funcs = funcs
	return
}

func (d *Decoder) decodeLocalConsts() ([]LocalConst, error) {
	n, err := d.decodeUint16()
	if err != nil {
		return nil, fmt.Errorf("consts length: %w", err)
	}

	var consts []LocalConst
	for i := uint16(0); i < n; i++ {
		name, err := d.decodeString()
		if err != nil {
			return nil, fmt.Errorf("decode local const name: %w", err)
		}
		value, err := d.decodeUint16()
		if err != nil {
			return nil, fmt.Errorf("decode local const value: %w", err)
		}
		consts = append(consts, LocalConst{
			Name:  name,
			Value: value,
		})
	}
	return consts, nil
}

func (d *Decoder) decodeLocalVars() ([]LocalVar, error) {
	n, err := d.decodeUint16()
	if err != nil {
		return nil, fmt.Errorf("vars length: %w", err)
	}

	var vars []LocalVar
	for i := uint16(0); i < n; i++ {
		name, err := d.decodeString()
		if err != nil {
			return nil, fmt.Errorf("decode local var name: %w", err)
		}
		value, err := d.decodeUint16()
		if err != nil {
			return nil, fmt.Errorf("decode local var value: %w", err)
		}
		usages, err := d.decodeUint16Array()
		if err != nil {
			return nil, fmt.Errorf("decode local var usages: %w", err)
		}
		vars = append(vars, LocalVar{
			Name:   name,
			Value:  value,
			Usages: usages,
		})
	}
	return vars, nil
}

func (d *Decoder) decodeLocalFunctions() ([]LocalFunction, error) {
	n, err := d.decodeUint16()
	if err != nil {
		return nil, fmt.Errorf("functions length: %w", err)
	}

	var funcs []LocalFunction
	for i := uint16(0); i < n; i++ {
		name, err := d.decodeString()
		if err != nil {
			return nil, fmt.Errorf("decode local function name: %w", err)
		}
		origin, err := d.decodeUint16()
		if err != nil {
			return nil, fmt.Errorf("decode local function origin: %w", err)
		}
		usages, err := d.decodeUint16Array()
		if err != nil {
			return nil, fmt.Errorf("decode local function usages: %w", err)
		}
		funcs = append(funcs, LocalFunction{
			Name:   name,
			Origin: origin,
			Usages: usages,
		})
	}
	return funcs, nil
}

func (d *Decoder) decodeCodeSection() []byte {
	if d.bytesLeft() == 0 {
		return nil
	}
	return d.buf[d.pos:]
}

func (d *Decoder) decodeImportedConsts() ([]ImportedConst, error) {
	n, err := d.decodeUint16()
	if err != nil {
		return nil, fmt.Errorf("consts length: %w", err)
	}

	var consts []ImportedConst
	for i := uint16(0); i < n; i++ {
		name, err := d.decodeString()
		if err != nil {
			return nil, fmt.Errorf("decode imported const name: %w", err)
		}
		usages, err := d.decodeUint16Array()
		if err != nil {
			return nil, fmt.Errorf("decode imported const usages: %w", err)
		}
		consts = append(consts, ImportedConst{
			Name:   name,
			Usages: usages,
		})
	}
	return consts, nil
}

func (d *Decoder) decodeImportedVars() ([]ImportedVar, error) {
	n, err := d.decodeUint16()
	if err != nil {
		return nil, fmt.Errorf("vars length: %w", err)
	}

	var vars []ImportedVar
	for i := uint16(0); i < n; i++ {
		name, err := d.decodeString()
		if err != nil {
			return nil, fmt.Errorf("decode imported var name: %w", err)
		}
		usages, err := d.decodeUint16Array()
		if err != nil {
			return nil, fmt.Errorf("decode imported var usages: %w", err)
		}
		vars = append(vars, ImportedVar{
			Name:   name,
			Usages: usages,
		})
	}
	return vars, nil
}

func (d *Decoder) decodeImportedFunctions() ([]ImportedFunction, error) {
	n, err := d.decodeUint16()
	if err != nil {
		return nil, fmt.Errorf("functions length: %w", err)
	}

	var funcs []ImportedFunction
	for i := uint16(0); i < n; i++ {
		name, err := d.decodeString()
		if err != nil {
			return nil, fmt.Errorf("decode imported function name: %w", err)
		}
		usages, err := d.decodeUint16Array()
		if err != nil {
			return nil, fmt.Errorf("decode imported function usages: %w", err)
		}
		funcs = append(funcs, ImportedFunction{
			Name:   name,
			Usages: usages,
		})
	}
	return funcs, nil
}

func (d *Decoder) decodeImportedModule() (ImportedModule, error) {
	name, err := d.decodeString()
	if err != nil {
		return ImportedModule{}, err
	}
	consts, err := d.decodeImportedConsts()
	if err != nil {
		return ImportedModule{}, err
	}
	vars, err := d.decodeImportedVars()
	if err != nil {
		return ImportedModule{}, err
	}
	funcs, err := d.decodeImportedFunctions()
	if err != nil {
		return ImportedModule{}, err
	}
	return ImportedModule{
		Name:   name,
		Consts: consts,
		Vars:   vars,
		Funcs:  funcs,
	}, nil
}
